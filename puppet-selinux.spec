Name:           puppet-selinux
Version:        1.1
Release:        2%{?dist}
Summary:        Puppet selinux  module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

# Upstream packages some files with 'python' as the interpreter
%undefine __brp_mangle_shebangs
# There is no 'python' on el8, so we need to instruct rpm-build to not
# Require: python
AutoReqProv: no

%description
Puppet selinux module, used by afs puppet module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/selinux/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/selinux/
touch %{buildroot}/%{_datadir}/puppet/modules/selinux/supporting_module

%files -n puppet-selinux
%{_datadir}/puppet/modules/selinux

%changelog
* Tue Oct 15 2024 Ben Morrice <ben.morrice@cern.ch> - 1.1-2
- Fix packaging bug on el8 (python)

* Tue Oct 15 2024 CERN Linux Droid <linux.ci@cern.ch> - 1.1-1
- Rebased to #1729dcb4 by locmap-updater

* Thu Oct 13 2022 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
- Initial release
